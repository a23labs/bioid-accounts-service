const Sequelize = require('sequelize');
const db = require('../config/db');

const Customer = db.define('Customers', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  firstname: {
    type: Sequelize.STRING
  },
  lastname: {
    type: Sequelize.STRING
  },
  age: {
    type: Sequelize.INTEGER
  }
})

module.exports = Customer;