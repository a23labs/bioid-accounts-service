const Sequelize = require('sequelize');
const db = require('../config/db');

const Department = db.define('Departments', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  department_code: {
    type: Sequelize.INTEGER
  }
})
module.exports = Department;