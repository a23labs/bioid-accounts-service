const Sequelize = require('sequelize');
const db = require('../config/db');
const Branch = db.define('Branches', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  places_id: {
    type: Sequelize.STRING
  },
  latlong: {
    type: Sequelize.STRING
  }
})
module.exports = Branch;