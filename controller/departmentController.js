const db = require('../config/db');
//const Customer = db.Customers;
const Department = require('../models/department');
// Post a Customer
exports.create = (req, res) => {	
	// Save to MySQL database
	Department.create({  
	  name: req.body.name,
	  description: req.body.description,
	  department_code: req.body.department_code
	}).then(department => {		
		// Send created customer to client
		res.send(department);
	});
};
 
// FETCH all Customers
exports.findAll = (req, res) => {
	Department.findAll().then(departments => {
	  // Send all customers to Client
	  res.send(departments);
	});
};

// Find a Customer by Id
exports.findById = (req, res) => {	
	Department.findById(req.params.departmentId).then(department => {
		res.send(department);
	})
};
 
// Update a Customer
exports.update = (req, res) => {
	const id = req.params.customerId;
	Department.update( { name: req.body.name, description: req.body.description, department_code: req.body.department_code }, 
					 { where: {id: req.params.departmentId} }
				   ).then(() => {
					 res.status(200).send("updated successfully a department with id = " + id);
				   });
};
 
// Delete a Customer by Id
exports.delete = (req, res) => {
	const id = req.params.departmentId;
	Department.destroy({
	  where: { id: id }
	}).then(() => {
	  res.status(200).send('deleted successfully a Department with id = ' + id);
	});
};