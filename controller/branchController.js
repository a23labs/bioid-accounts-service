const db = require('../config/db');
//const branch = db.Customers;
const Branch = require('../models/branch');
// Post a branch
exports.create = (req, res) => {	
	// Save to MySQL database
	Branch.create({  
	  name: req.body.name,
	  description: req.body.description,
		places_id: req.body.places_id,
		latlong:req.body.latlong
	}).then(branch => {		
		// Send created branch to client
		res.send(branch);
	});
};
 
// FETCH all Customers
exports.findAll = (req, res) => {
	Branch.findAll().then(branches => {
	  // Send all customers to Client
	  res.send(branches);
	});
};

// Find a Customer by Id
exports.findById = (req, res) => {	
	Branch.findById(req.params.branchId).then(branch => {
		res.send(branch);
	})
};
 
// Update a Customer
exports.update = (req, res) => {
	const id = req.params.customerId;
	Branch.update( { name: req.body.name, description: req.body.description, places_id: req.body.places_id,latlong: req.body.latlong }, 
					 { where: {id: req.params.branchId} }
				   ).then(() => {
					 res.status(200).send("updated successfully a Branch with id = " + id);
				   });
};
 
// Delete a Customer by Id
exports.delete = (req, res) => {
	const id = req.params.branchId;
	Branch.destroy({
	  where: { id: id }
	}).then(() => {
	  res.status(200).send('deleted successfully a Branch with id = ' + id);
	});
};