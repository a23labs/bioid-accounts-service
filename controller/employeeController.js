const db = require('../config/db');
//const Customer = db.Customers;
const Employee = require('../models/employee');
// Post a Customer
exports.create = (req, res) => {	
	// Save to MySQL database
	Employee.create({  
	  fist_name: req.body.fist_name,
	  last_name: req.body.last_name,
		appointment_date: req.body.appointment_date,
		department_id: req.body.department_id,
	  branch_id: req.body.branch_id,
		next_of_kin_contact: req.body.next_of_kin_contact,
		next_of_kin_name: req.body.next_of_kin_name,
	  mm_is_confirmed: req.body.mm_is_confirmed,
		gender: req.body.gender,
		job_title: req.body.job_title,
	  home_address: req.body.home_address,
		profile_photo: req.body.profile_photo,
		id_scan: req.body.id_scan,
	  thumb_fp: req.body.thumb_fp,
		index_fp: req.body.index_fp,
		contact: req.body.contact,
	  username: req.body.username,
		enabled: req.body.enabled,
		mm_number: req.body.mm_number,
		account_name: req.body.account_name,
	  compressed_image: req.body.compressed_image,
		email_address: req.body.email_address,
		is_branch_manager: req.body.is_branch_manager,
	  work_email_address: req.body.work_email_address,
		deletion_reason: req.body.deletion_reason,
		closed_by: req.body.closed_by,
	  closed_at: req.body.closed_at,
	  last_date_of_work: req.body.last_date_of_work
	}).then(employee => {		
		// Send created customer to client
		res.send(employee);
	});
};
 
// FETCH all Customers
exports.findAll = (req, res) => {
	Employee.findAll().then(employees => {
	  // Send all customers to Client
	  res.send(employees);
	});
};

// Find a Customer by Id
exports.findById = (req, res) => {	
	Employee.findById(req.params.employeeId).then(employee => {
		res.send(employee);
	})
};
 
// Update a Customer
exports.update = (req, res) => {
	const id = req.params.employeeId;
	Employee.update( { 
		fist_name: req.body.fist_name,
	  last_name: req.body.last_name,
		appointment_date: req.body.appointment_date,
		department_id: req.body.department_id,
	  branch_id: req.body.branch_id,
		next_of_kin_contact: req.body.next_of_kin_contact,
		next_of_kin_name: req.body.next_of_kin_name,
	  mm_is_confirmed: req.body.mm_is_confirmed,
		gender: req.body.gender,
		job_title: req.body.job_title,
	  home_address: req.body.home_address,
		profile_photo: req.body.profile_photo,
		id_scan: req.body.id_scan,
	  thumb_fp: req.body.thumb_fp,
		index_fp: req.body.index_fp,
		contact: req.body.contact,
	  username: req.body.username,
		enabled: req.body.enabled,
		mm_number: req.body.mm_number,
		account_name: req.body.account_name,
	  compressed_image: req.body.compressed_image,
		email_address: req.body.email_address,
		is_branch_manager: req.body.is_branch_manager,
	  work_email_address: req.body.work_email_address,
		deletion_reason: req.body.deletion_reason,
		closed_by: req.body.closed_by,
	  closed_at: req.body.closed_at,
	  last_date_of_work: req.body.last_date_of_work }, 
		{ where: {id: req.params.employeeId} 
	} 
		).then(() => { res.status(200).send("updated successfully a department with id = " + id);
				   });
};
// Delete a Customer by Id
exports.delete = (req, res) => {
	const id = req.params.employeeId;
	Employee.destroy({
	  where: { id: id }
	}).then(() => {
	  res.status(200).send('deleted successfully a employee with id = ' + id);
	});
};