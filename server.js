var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())
 
const db = require('./config/db.js');
  
// force: true will drop the table if it already exists

/* db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
}); */
 
require('./route/customer.route.js')(app);
require('./route/department.route')(app);
require('./route/branch.route')(app);
require('./route/employee.route')(app);
// Create a Server

app.listen(3000);
console.log('listening to 3000');