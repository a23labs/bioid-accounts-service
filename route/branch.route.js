module.exports = function(app) {
 
    const branches = require('../controller/branchController');
 
    // Create a new department
    app.post('/api/branches', branches.create);
 
    // Retrieve all department
    app.get('/api/branches', branches.findAll);
 
    // Retrieve a single department by Id
    app.get('/api/branches/:branchId', branches.findById);
 
    // Update a department with Id
    app.put('/api/branches/:branchId', branches.update);
 
    // Delete a department with Id
    app.delete('/api/branches/:branchId', branches.delete);
}