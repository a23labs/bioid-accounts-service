module.exports = function(app) {
 
    const departments = require('../controller/departmentController');
 
    // Create a new department
    app.post('/api/departments', departments.create);
 
    // Retrieve all department
    app.get('/api/departments', departments.findAll);
 
    // Retrieve a single department by Id
    app.get('/api/departments/:departmentId', departments.findById);
 
    // Update a department with Id
    app.put('/api/departments/:departmentId', departments.update);
 
    // Delete a department with Id
    app.delete('/api/departments/:departmentId', departments.delete);
}