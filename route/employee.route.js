module.exports = function(app) {
 
    const employees = require('../controller/employeeController');
 
    // Create a new department
    app.post('/api/employees', employees.create);
 
    // Retrieve all department
    app.get('/api/employees', employees.findAll);
 
    // Retrieve a single department by Id
    app.get('/api/employees/:employeeId', employees.findById);
 
    // Update a department with Id
    app.put('/api/employees/:employeeId', employees.update);
 
    // Delete a department with Id
    app.delete('/api/employees/:employeeId', employees.delete);
}