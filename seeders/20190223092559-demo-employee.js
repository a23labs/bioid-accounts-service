'use strict';
var datetime = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Employees', [{
       first_name:'John',
       last_name:'Doe',
       appointment_date:'1/1/2019', 
       department_id:'1',
       branch_id:'1',
       next_of_kin_contact:'0771234567',
       next_of_kin_name:'Jane Doe',
       mm_is_confirmed:'boolean',
       gender:'male',
       job_title:'manager',
       home_address:'kampala',
       profile_photo:'photo_string',
       id_scan:"id_photo",
       thumb_fp:"thumb_signature",
       index_fp:'index_signature',
       contact:'0771234567',
       username:'jone',
       enabled:'boolean',
       mm_number:'0771234567',
       account_name:'jane',
       compressed_image:'image_string',
       email_address:'jane@gmail.com',
       is_branch_manager:'boolean',
       work_email_address:'jane@work.com',
       deletion_reason:'reason',
       closed_by:'username',
       closed_at:'3/1/2019',
       last_date_of_work:'2/1/2019',
       createdAt: datetime,
       updatedAt: datetime
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Employees', null, {});
  }
};
