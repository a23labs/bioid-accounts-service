'use strict';
var datetime = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Departments', [{
        name: 'Department',
        description: 'Department Description ',
        department_code: '00',
        createdAt: datetime,
        updatedAt: datetime
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Departments', null, {});
  }
};
