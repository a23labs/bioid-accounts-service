'use strict';
var datetime = new Date();
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Customers', [{
        firstname: 'Jave',
        lastname: 'Dove',
        age: '13',
        createdAt: datetime,
        updatedAt: datetime
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Customers', null, {});
  }
};
